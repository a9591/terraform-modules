

variable "instance_ami" {
    type        = string
    description = "Ec2 instance ami"
}

variable "instance_type" {
    type        = string
    default     = "t2.micro"
    description = "instance type"
}


