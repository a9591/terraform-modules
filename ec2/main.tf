locals {
    
    subnet_id = "subnet-009de3ec6ff9e5085"
}

resource "aws_instance" "web" {
  ami           = var.instance_ami
  instance_type = var.instance_type
  tags          = {
                     Name = "terraform_ec2"
  }
  subnet_id = local.subnet_id
  user_data = <<-EOF
   #!/bin/bash
   echo "Hello World" >> index.html
   nohub busybox httpd -f -p 80 &
EOF
  
}